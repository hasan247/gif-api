<?php


class GifControllerTest extends TestCase
{
    /**
     * Test that search query 'b' returns a gif response for banana
     *
     * @return void
     */
    public function testSearch()
    {
        $this->json('GET', '/v1/gifs/search?q=b', [], ['API_KEY' => ''])
            ->seeJson([
                'data' => [
                    [
                        'title' => 'Banana',
                        'url' => 'https://www.gifapi.com/banana.gif',
                    ]
                ],
            ]);
    }

    /**
     * Test that random endpoint returns a gif in the reponse
     *
     * @return void
     */
    public function testRandom()
    {
        $this->json('GET', '/v1/gifs/random', [], ['API_KEY' => '']);

        $response = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKey('title', $response['data']);
        $this->assertArrayHasKey('url', $response['data']);
    }
}
