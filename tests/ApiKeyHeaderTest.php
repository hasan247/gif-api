<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response;

class ApiKeyHeaderTest extends TestCase
{
    /**
     * Test that omitting the API_KEY header results in a 401 response
     *
     * @return void
     */
    public function testUnauthorisedRequest()
    {
        $response = $this->call('GET', '/v1/gifs/random');

        $this->assertEquals(
            Response::HTTP_UNAUTHORIZED, $response->status()
        );
    }

    /**
     * Test that including the API_KEY header results in a 200 response
     *
     * @return void
     */
    public function testAuthorisedRequest()
    {
        $this->json('GET', '/v1/gifs/random', [], ['API_KEY' => ''])
            ->seeJson();
    }
}
