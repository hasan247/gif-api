<?php


namespace App\EntityTransformers;


abstract class AbstractTransformer
{
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);
}
