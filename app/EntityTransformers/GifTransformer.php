<?php


namespace App\EntityTransformers;


use App\Entities\Gif;

class GifTransformer extends AbstractTransformer
{
    /**
     * @param Gif $item
     * @return array
     */
    public function transform($item)
    {
        return [
            'title' => $item->getTitle(),
            'url' => $item->getUrl(),
        ];
    }
}
