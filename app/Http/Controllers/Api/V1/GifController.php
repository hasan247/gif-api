<?php


namespace App\Http\Controllers\Api\V1;


use App\EntityTransformers\GifTransformer;
use App\Http\Controllers\Controller;
use App\Services\GifService;
use Illuminate\Http\Request;

class GifController extends Controller
{
    /**
     * @var GifService
     */
    protected $gifService;

    /**
     * @var GifTransformer
     */
    protected $gifTransformer;

    /**
     * GifController constructor.
     * @param GifService $gifService
     * @param GifTransformer $gifTransformer
     */
    public function __construct(GifService $gifService, GifTransformer $gifTransformer)
    {
        $this->gifService = $gifService;
        $this->gifTransformer = $gifTransformer;
    }

    public function search(Request $request)
    {
        $query = $request->input('q');
        $gifs = $this->gifService->search($query);

        return response()->json([
            'data' => $this->gifTransformer->transformCollection($gifs),
        ]);
    }

    public function random()
    {
        $gif = $this->gifService->random();

        return response()->json([
            'data' => $this->gifTransformer->transform($gif),
        ]);
    }
}
