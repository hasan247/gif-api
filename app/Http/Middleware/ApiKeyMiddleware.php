<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // ensure that each request contains an API_KEY or APIKEY header (underscores do not work in headers on heroku)
        $header = $request->header('API_KEY', $request->header('APIKEY', false));

        // strict check to allow blank header value
        if ($header === false) {
            return response('Unauthorized, please send an API_KEY or APIKEY header', 401);
        }
        return $next($request);
    }
}
