<?php


namespace App\Services;


use App\Entities\Gif;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class GifService
{
    /**
     * @var Gif[]
     */
    protected $gifs;

    /**
     * GifService constructor.
     */
    public function __construct()
    {
        $this->gifs = [
            new Gif('Apple', 'https://www.gifapi.com/apple.gif'),
            new Gif('Banana', 'https://www.gifapi.com/banana.gif'),
            new Gif('Clementine', 'https://www.gifapi.com/clementine.gif'),
            new Gif('Dragon Fruit', 'https://www.gifapi.com/dragon-fruit.gif'),
            new Gif('Elderberry', 'https://www.gifapi.com/elderberry.gif'),
            new Gif('Fig', 'https://www.gifapi.com/fig.gif'),
            new Gif('Guava', 'https://www.gifapi.com/guava.gif'),
            new Gif('Honeydew Melon', 'https://www.gifapi.com/honeydew-melon.gif'),
            new Gif('Indian Prune', 'https://www.gifapi.com/indian-prune.gif'),
            new Gif('Jackfruit', 'https://www.gifapi.com/jackfruit.gif'),
            new Gif('Kiwi', 'https://www.gifapi.com/kiwi.gif'),
            new Gif('Lime', 'https://www.gifapi.com/lime.gif'),
            new Gif('Mango', 'https://www.gifapi.com/mango.gif'),
            new Gif('Nectarine', 'https://www.gifapi.com/nectarine.gif'),
            new Gif('Orange', 'https://www.gifapi.com/orange.gif'),
            new Gif('Pineapple', 'https://www.gifapi.com/pineapple.gif'),
            new Gif('Quince', 'https://www.gifapi.com/quince.gif'),
            new Gif('Raspberry', 'https://www.gifapi.com/raspberry.gif'),
            new Gif('Strawberry', 'https://www.gifapi.com/strawberry.gif'),
            new Gif('Tangerine', 'https://www.gifapi.com/tangerine.gif'),
            new Gif('Ugni', 'https://www.gifapi.com/ugni.gif'),
            new Gif('Voavanga', 'https://www.gifapi.com/voavanga.gif'),
            new Gif('Watermelon', 'https://www.gifapi.com/watermelon.gif'),
            new Gif('Ximenia', 'https://www.gifapi.com/ximenia.gif'),
            new Gif('Yellow Passionfruit', 'https://www.gifapi.com/yellow-passionfruit.gif'),
            new Gif('Zig Zag Vine', 'https://www.gifapi.com/zig-zag-vine.gif'),
        ];
    }

    /**
     * @param string $query
     *
     * @return Gif[]
     */
    public function search($query)
    {
        // sanitise/standardise query
        $query = trim(strtolower($query));

        return array_values(
            array_filter($this->gifs, function (Gif $gif) use ($query) {
                $title = strtolower($gif->getTitle());

                return Str::startsWith($title, $query);
            })
        );
    }

    /**
     * @return Gif
     */
    public function random()
    {
        return Arr::random($this->gifs);
    }
}
