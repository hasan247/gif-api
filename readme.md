# gif-api

For convenience, this API has been deployed to [https://gif-api-qtc.herokuapp.com/](https://gif-api-qtc.herokuapp.com/).

## Requirements

- PHP 7.1 or greater
- composer

## Setup
1. Run `composer install`
2. To serve using the php development server, run ` php -S localhost:8000 -t public`

## Tests
Run `./vendor/bin/phpunit`
