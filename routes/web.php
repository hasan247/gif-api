<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Middleware\ApiKeyMiddleware;

$router->group([
    'namespace' => 'Api\V1',
    'prefix' => 'v1',
    'middleware' => ApiKeyMiddleware::class,
], function () use ($router) {
    $router->group([
        'prefix' => 'gifs'
    ], function () use ($router) {
        $router->get('search', [
            'as' => 'searchGifs',
            'uses' => 'GifController@search'
        ]);

        $router->get('random', [
            'as' => 'randomGif',
            'uses' => 'GifController@random'
        ]);
    });
});
